package se325.lab04.concert.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se325.lab04.concert.domain.Concert;

/**
 * Class to implement a simple REST Web service for managing Concerts.
 * <p>
 * ConcertResource implements a WEB service with the following interface:
 * - GET    <base-uri>/concerts/{id}
 * Retrieves a Concert based on its unique id. The HTTP response
 * message has a status code of either 200 or 404, depending on
 * whether the specified Concert is found.
 * <p>
 * - GET    <base-uri>/concerts?start&size
 * Retrieves a collection of Concerts, where the "start" query
 * parameter identifies an index position, and "size" represents the
 * max number of successive Concerts to return. The HTTP response
 * message returns 200.
 * <p>
 * - POST   <base-uri>/concerts
 * Creates a new Concert. The HTTP post message contains a
 * representation of the Concert to be created. The HTTP Response
 * message returns a Location header with the URI of the new Concert
 * and a status code of 201.
 * <p>
 * - DELETE <base-uri>/concerts
 * Deletes all Concerts, returning a status code of 204.
 * <p>
 * Where a HTTP request message doesn't contain a cookie named clientId
 * (Config.CLIENT_COOKIE), the Web service generates a new cookie, whose value
 * is a randomly generated UUID. The Web service returns the new cookie as part
 * of the HTTP response message.
 */
@Path("/concerts")
public class ConcertResource {

    private static Logger LOGGER = LoggerFactory.getLogger(ConcertResource.class);
    private static long idCounter = 0;

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON, SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT})
    public Response retrieveConcert(@PathParam("id") long id) {
    	LOGGER.info("Retrieving concert: "+id);
    	
    	EntityManager em = PersistenceManager.instance().createEntityManager();
    	
        em.getTransaction().begin();
        Concert concert = em.find(Concert.class, id);
        em.getTransaction().commit();
        em.close();
        
        if (concert == null) {
            return Response.status(Status.NOT_FOUND)
            		.build();
        }
        
        return Response.ok(concert)
        		.build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT})
    public Response retrieveConcerts(
            @QueryParam("start") long start,
            @QueryParam("size") int size) {
    	LOGGER.info("Retrieving all concerts!");
    	EntityManager em = PersistenceManager.instance().createEntityManager();
        
    	List<Concert> concerts = new ArrayList<>();
        GenericEntity<List<Concert>> entity = new GenericEntity<List<Concert>>(concerts) {};
        
        em.getTransaction().begin();
        TypedQuery<Concert> concertQuery = em.createQuery("select c from Concert c", Concert.class);
        concerts = concertQuery.getResultList();
        em.getTransaction().commit();
        em.close();
        
        return Response.ok(entity)
        		.build();
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON, SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT})
    public Response updateConcert(Concert concert) {
    	EntityManager em = PersistenceManager.instance().createEntityManager();
    	
        Concert newConcert = new Concert(concert.getId(), concert.getTitle(), concert.getDate(), concert.getPerformer());
        LOGGER.info("Putting a concert "+newConcert.toString());
        
        em.getTransaction().begin();
        em.merge(newConcert);
        em.getTransaction().commit();
        em.close();
        
        return Response.noContent()
        		.build();
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON, SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT})
    public Response createConcert(Concert concert) {
    	EntityManager em = PersistenceManager.instance().createEntityManager();
    	
        long id = idCounter++;
        Concert newConcert = new Concert(id, concert.getTitle(), concert.getDate(), concert.getPerformer());
        LOGGER.info("Adding a concert "+newConcert.toString());
        
        em.getTransaction().begin();
        em.merge(newConcert);
        em.getTransaction().commit();
        em.close();
        
        return Response.created(URI.create("/concerts/" + newConcert.getId()))
        		.build();
    }

    @DELETE
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_JSON, SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT})
    public Response deleteConcert(@PathParam("id") long id) {
    	LOGGER.info("Removing single concert!");
    	EntityManager em = PersistenceManager.instance().createEntityManager();
    	
    	em.getTransaction().begin();
        em.remove(em.find(Concert.class, id));
        em.getTransaction().commit();
        em.close();
        
        return Response.noContent()
        		.build();
    }
    
    @DELETE
    public Response deleteAllConcerts() {
    	LOGGER.info("Removing all concerts!");
    	EntityManager em = PersistenceManager.instance().createEntityManager();
    	
    	em.getTransaction().begin();
        TypedQuery<Concert> concertQuery = em.createQuery("select c from Concert c", Concert.class);
        List<Concert> concerts = concertQuery.getResultList();
        for(Concert c: concerts) {
        	em.remove(c);
        	LOGGER.info(c.toString()+" deleted!");
        }
        em.getTransaction().commit();
        em.close();
        
        return Response.noContent()
        		.build();
    }
}